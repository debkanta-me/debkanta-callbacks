const fs = require('fs');
const path = require('path');

function callback(listID, cb) {
    if (typeof cb != 'function' && typeof listID != "function") {
        console.log(new Error('cb is not passed!'));
    } else if (typeof listID != 'string') {
        listID(new Error("listID is not passed!"));
    } else {
        setTimeout(() => {
            fs.readFile(path.join(__dirname, 'cards.json'), 'utf8', (err, cards) => {
                if (err) {
                    cb(err);
                } else {
                    try {
                        cards = JSON.parse(cards);

                        const result = cards[listID];

                        if (typeof result != "undefined") {
                            cb(null, result);
                        } else {
                            cb(new Error("No data found for the corresponding listID!"));
                        }
                    } catch (e) {
                        cb(new Error("Some error occured while parsing JSON!"));
                    }
                }
            })
        }, 2 * 1000);
    }
}

module.exports = callback;