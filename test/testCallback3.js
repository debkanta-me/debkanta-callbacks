const callback = require("../callback3");

//If both the arguments are correct and passed!
callback("ghnb768", (err, cards) => {
    if (err) {
        console.error("Error occured!");
        console.error(err);
    } else {
        console.log("Successfully executed!");
        console.log(cards);
    }
})

//If both the arguments are passed but boardID is not present in boards
callback("ghnb76", (err, cards) => {
    if (err) {
        console.error("Error occured!");
        console.error(err);
    } else {
        console.log("Successfully executed!");
        console.log(cards);
    }
})

//If both the first argument is missing
callback((err, cards) => {
    if (err) {
        console.error("Error occured!");
        console.error(err);
    } else {
        console.log("Successfully executed!");
        console.log(cards);
    }
})

//If callback is missing
callback("ghnb768");
