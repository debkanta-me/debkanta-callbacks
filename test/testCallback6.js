const callback = require("../callback6");

//If the argument is passed correctly
callback("Thanos");

//If no argument is passed
callback();

//If the passed boardName is not present in the .json file
callback("Thannnos");
