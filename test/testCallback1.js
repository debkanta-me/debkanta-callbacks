const callback = require("../callback1");

//If both the arguments are correct and passed!
callback("mcu453ed", (err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data.name);
    }
})

//If both the arguments are passed but boardID is not present in boards
callback("mcu453e", (err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data.name);
    }
})

//If both the first argument is missing
callback((err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data.name);
    }
})

//If callback is missing
callback("mcu453ed")