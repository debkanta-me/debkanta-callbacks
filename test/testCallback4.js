const callback = require("../callback4");

//If the arguments are passed correctly
callback("Thanos", "Mind");

//If one of the arguments is missing or no arguments are passed
callback();

//If the boardName or listName is not present in the .json file
callback("Tanos", "Minddd");