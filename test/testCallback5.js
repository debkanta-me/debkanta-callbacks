const callback = require("../callback5");

//If the arguments are passed correctly
callback("Thanos", "Mind", "Space");

//If one of the arguments is not or no arguments are passed
callback();

//If the boardName and listName is not present in .json file
callback("Thanoss", "Mindd", "Sppace");

