const callback = require("../callback2");

//If both the arguments are correct and passed!
callback("abc122dc", (err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data);
    }
})

//If both the arguments are passed but boardID is not present in boards
callback("abc122d", (err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data);
    }
})

//If both the first argument is missing
callback((err, data) => {
    if (err) {
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Successfully executed!");
        console.log(data);
    }
})

//If callback is missing
callback("abc122dc")