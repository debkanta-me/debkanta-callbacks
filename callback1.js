const { error } = require('console');
const fs = require('fs')
const path = require('path')

function callback(boardID, cb) {
    if(typeof cb != 'function' && typeof boardID != "function"){
        console.log(new Error('cb is not passed!'));
    }else if (typeof boardID != 'string') {
        boardID(new Error("boardID is not found"));
    } else {
        setTimeout(() => {
            fs.readFile(path.join(__dirname, 'boards.json'), 'utf8', (err, boards) => {
                if (err) {
                    cb(err);
                } else {
                    try {
                        boards = JSON.parse(boards);
                        
                        const result = boards.find(element => boardID === element.id);

                        if(typeof result != "undefined"){
                            cb(null, result);
                        }else{
                            cb(new Error("No data found for the corresponding boardID!"));
                        }
                    } catch (e) {
                        cb(new Error("Some error occured while parsing JSON!"));
                    }
                }
            })
        }, 2 * 1000);
    }
}

module.exports = callback;