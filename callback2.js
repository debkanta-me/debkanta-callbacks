const fs = require('fs');
const path = require('path');

function callback(boardID, cb) {
    if (typeof cb != 'function' && typeof boardID != "function") {
        console.log(new Error('cb is not passed!'));
    }else if (typeof boardID != 'string') {
        boardID(new Error("boardID is not found"));
    } else {
        setTimeout(() => {
            fs.readFile(path.join(__dirname, 'lists.json'), 'utf8', (err, lists) => {
                if (err) {
                    cb(err);
                } else {
                    try {
                        lists = JSON.parse(lists);

                        const result = lists[boardID];

                        if(typeof result != "undefined"){
                            cb(null, result);
                        }else{
                            cb(new Error("No data found for the corresponding listID!"));
                        }
                    } catch {
                        cb(new Error("Some error occured while parsing JSON data!"));
                    }
                }

            })
        }, 2 * 1000);
    }
}

module.exports = callback;