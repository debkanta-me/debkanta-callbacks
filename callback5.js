const fs = require("fs");
const path = require("path");
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function callback(boardName, listName1, listName2) {
    if (typeof boardName != "string" || typeof listName1 != "string" || typeof listName2 != "string") {
        console.log(new Error("Pass valid arguments!"));
    } else {
        setTimeout(() => {
            fs.readFile(path.join(__dirname, "boards.json"), 'utf8', (err, boards) => {
                if (err) {
                    console.log(err);
                } else {
                    try {
                        boards = JSON.parse(boards)
                        const board = boards.find(element => element.name === boardName);

                        if (!board) {
                            console.log(new Error("No board is found with the BoardName passed!"));
                        } else {
                            callback1(board.id, (err, boardInfo) => {
                                if (err) {
                                    console.error("Error occured!");
                                    console.error(err);
                                } else {
                                    console.log("Successfully executed!");
                                    console.log(board);

                                    callback2(board.id, (err, lists) => {
                                        if (err) {
                                            console.error("Error occured!");
                                            console.error(err);
                                        } else {
                                            console.log("Successfully executed!");
                                            console.log(lists);

                                            const cards = lists.filter(el => el.name === listName1 || el.name === listName2);

                                            cards.forEach((card) => {
                                                if (!card) {
                                                    console.log(new Error("No card found with the listName passed!"));
                                                } else {
                                                    callback3(card.id, (err, data) => {
                                                        if (err) {
                                                            console.error("Error occured!");
                                                            console.error(err);
                                                        } else {
                                                            console.log("Successfully executed!");
                                                            console.log(data);
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            })
        }, 2 * 1000);
    }
}

module.exports = callback;
