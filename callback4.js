const fs = require("fs")
const path = require("path")
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function callback(boardName, listName) {
    if (typeof boardName != 'string' || typeof listName != 'string') {
        console.log(new Error("Pass valid arguments!"));
    } else {
        setTimeout(() => {
            fs.readFile(path.join(__dirname, "boards.json"), 'utf8', (err, boards) => {
                if (err) {
                    console.log(err);
                } else {
                    try {
                        boards = JSON.parse(boards)
                        const board = boards.find(element => element.name === boardName);

                        if (!board) {
                            console.log(new Error("No board found with the BoardName passed!"));
                        } else {
                            callback1(board.id, (err, boardInfo) => {
                                if (err) {
                                    console.error("Error occured!");
                                    console.error(err);
                                } else {
                                    console.log("Successfully executed!");
                                    console.log(boardInfo);

                                    callback2(board.id, (err, lists) => {
                                        if (err) {
                                            console.error("Error occured!");
                                            console.error(err);
                                        } else {
                                            console.log("Successfully executed!");
                                            console.log(lists);

                                            const card = lists.find(el => el.name === listName);

                                            if (!card) {
                                                console.log(new Error("No card found with the listName passed!"));
                                            } else {
                                                callback3(card.id, (err, card) => {
                                                    if (err) {
                                                        console.error("Error occured!");
                                                        console.error(err);
                                                    } else {
                                                        console.log("Successfully executed!");
                                                        console.log(card);
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            })

        }, 2 * 1000);
    }
}

module.exports = callback;
